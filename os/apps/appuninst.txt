--@name UninstallerApp
--@author Yuri6037

--[[
Copyright (c) 2021 Yuri6037

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
]]--

local app = {}

function app:Init()
	self:SetSize(256, 166)
	self:SetTitle("Uninstall application")
	self:DisableDecorations()
	self:SetPos(LOCATION_CENTER)

	local lst = self:AddComponent("listbox", 10, 32)
	lst:SetSize(236, 100)
	lst:SetMaxLines(5)
	lst:SetData(OS.AppList())
	local bad = self:AddComponent("button", 128, 140, "Cancel", function()
		self:Close()
	end)
	bad:SetSize(128, 16)
	local ok = self:AddComponent("button", 0, 140, "Uninstall", function()
		local id, str = lst:GetSelected()
		if (OS.UndefineApp(str)) then
			OS.TaskKill(str)
			lst:SetData(OS.AppList())
		end
	end)
	ok:SetSize(128, 16)
end

OS.DefineApp("uninstaller", app, true)
OS.AddAppMenuItem("uninstaller", "Uninstaller", "Tools")