--@name SF_EmitterKernelExtension
--@author Yuri6037

--[[
Copyright (c) 2021 Yuri6037

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
]]--

--Mouse handling vars
local FrameMsX = nil
local FrameMsY = nil
local prevX = 0
local prevY = 0

local KernelVars = OS.GetKernelVars()

local Player = KernelVars.Player
local Self = KernelVars.Self

if (OS.IsEmitter()) then
	local Scale = 0.2
	ScrW = 1024
	ScrH = 512

	function OS.MousePos()
		return FrameMsX, FrameMsY
	end

	local function MainRTDrawer()
		render.clear(0, 0, 0, 0)
		if (FrameMsX == nil or FrameMsY == nil) then
			FrameMsX, FrameMsY = prevX, prevY
			KernelVars.IsOnScr = false
		else
			prevX, prevY = FrameMsX, FrameMsY
			KernelVars.IsOnScr = true
		end
		if (not(KernelVars.CurScreen == nil)) then
			KernelVars.CurScreen:Render()
			KernelVars.CurScreen:Update(time.frameTime())
		end
	end
	local MainRT = rendertarget.create(false, ScrW, ScrH)
	local function SFEmitterMainRender()
		if (KernelVars.KillRender) then return end
		MainRT:drawToTexture(MainRTDrawer)
		local pos = Self:pos() + Self:up() * 110 + Self:right() * -(ScrW * Scale) / 2
		local ang = Self:getAngles()
		ang:RotateAroundAxis(Self:right(), 90)
		ang:RotateAroundAxis(Self:forward(), 90)
		FrameMsX, FrameMsY = util.MousePos3D2D(pos, ang, Scale)
		render.start3D2D(pos, ang, Scale)
			MainRT:draw(0, 0, ScrW, ScrH)
			render.setColor(255, 0, 0)
			render.drawRect(FrameMsX, FrameMsY, 4, 4)
		render.end3D2D()
	end
	hook("render", "LanteansOS_Render", SFEmitterMainRender)

	local Hooked = true
	timer.create("LanteansOS_RenderCheckArea", 1, 0, function()
		if (Player:pos():DistToSqr(Self:pos()) >= 30000 and Hooked) then
			Parametters["LoadTXT"] = "Reconnecting..."
			OS.SwitchScreen("boot")
			hook.remove("render", "LanteansOS_Render")
			MainRT:freeRT()
			Hooked = false
			net.SendPacket("DISCONNECT")
		elseif (Player:pos():DistToSqr(Self:pos()) < 30000 and not(Hooked)) then
			hook("render", "LanteansOS_Render", SFEmitterMainRender)
			MainRT = rendertarget.create(false, ScrW, ScrH)
			Hooked = true
			net.SendPacket("CONNECT")
		end
	end)

	net.SendPacket("CONNECT")
end

