--@name AIPermissionsApp
--@author Yuri6037

--[[
Copyright (c) 2021 Yuri6037

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
]]--

local Ico32 = {
    {
        Color = COLOR(255, 255, 0),
        Vertices = {
            VERTEX(8, 0),
            VERTEX(24, 0),
            VERTEX(24, 20),
            VERTEX(16, 32),
            VERTEX(8, 20)
        }
    }
}

local Ico16 = {
    {
        Color = COLOR(255, 255, 0),
        Vertices = {
            VERTEX(2, 0),
            VERTEX(14, 0),
            VERTEX(14, 10),
            VERTEX(8, 16),
            VERTEX(2, 10)
        }
    }
}

local app = {}
function app:Init()
    self:SetTitle("AI Permissions")
    self:SetSize(300, 140)
    self:SetPos(LOCATION_CENTER)
    self:SetIcon32(Ico32)
    self:SetIcon16(Ico16)

    self.DType = "All"
    local lst = {}
    for k, v in pairs(find.allPlayers()) do
    	table.insert(lst, v:name())
    end

    self.List = self:AddComponent("listbox", 10, 32)
    self.List:SetSize(280, 98)
    self.List:SetMaxLines(4)
	self.List:SetData(lst)

    self:AddPullDownMenu("Player Actions")
    self:AddPullDownMenuItem("Promote", function()
    end)
    self:AddPullDownMenuItem("Demote", function()
    end)
    self:AddPullDownMenuItem("Get rank", function()
    end)
    self:AddPullDownMenu("List display")
    self:AddPullDownMenuItem("All", function()
    	if (self.DType == "All") then return end
    	lst = {}
    	for k, v in pairs(find.allPlayers()) do
    		table.insert(lst, v:name())
    	end
    	self.List:SetData(lst)
    	self.DType = "All"
    end)
	self:AddPullDownMenuItem("Ranked", function()
		if (self.DType == "Ranked") then return end

		--self.DType = "Ranked"
	end)
end

OS.DefineApp("aiperms", app)
OS.AddAppMenuItem("aiperms", "AI Permissions", "LanAI", Ico16)